var score;
var playing = false;
var timeremaining;
var countdown;
var correctans;

/*HELPER*/
function setText(id,text){
    document.getElementById(id).innerHTML = text;
}

function show(id){
    document.getElementById(id).style.display = 'block';
}
function hide(id){
    document.getElementById(id).style.display = 'none';
}
document.getElementById("startreset").onclick = function(){
    if(playing == true){
        //playing is on and you tries to reset it!
        playing = false;
        window.location.reload();
    }else{
        //game is off and you want to start it!
        playing = true;
        score = 0;
        setText("scoreValue",score);
        
        show("timeremaining");
        timeremaining = 5;
        setText("timeremainingValue",timeremaining);
        
        this.innerHTML = "Reset Game";
        hide("gameover");
        startCountdown();
        generateQA();
    }
}

function startCountdown(){
    countdown = setInterval(function(){
        timeremaining -= 1;
        setText("timeremainingValue",timeremaining);
        if(timeremaining<=0){
            stopCountdown();
            show("gameover");
            playing = false;
            setText("startreset","Start Game!");
            hide("timeremaining");
            setText("scoreValue","");
            setText("gameover","<p>Game Over!</p><p>Your Score is "+score+"</p>");
        }
    },1000);
}

function stopCountdown(){
    clearInterval(countdown);
}

function generateQA(){
    var x = (1 + Math.round(Math.random() * 9));
    var y = (1 + Math.round(Math.random() * 9));
    correctans = x*y;
    setText("question",x+"x"+y);
    
    var correctPosition = (1 + Math.round(Math.random() * 3));
    setText("box"+correctPosition,correctans);
    
    var answers = [correctans];
    for(i=1;i<5;i++){
        var wrongans;
        if(i!=correctPosition){
            
            do{
                wrongans = (1 + Math.round(Math.random() * 9)) * (1 + Math.round(Math.random() * 9));
            }while(answers.indexOf(wrongans)>-1);
            
            answers.push(wrongans);
            setText("box"+i,wrongans);
        }
    }
}

for(i=1;i<5;i++){
    document.getElementById("box"+i).onclick = function(){
        if(playing){
            if(correctans == this.innerHTML){
                score++;
                setText("scoreValue",score);
                show("correct");
                hide("wrong");
                setTimeout(function(){
                    hide("correct");
                },1000);
                
                generateQA();
            }
            else{
                show("wrong");
                hide("correct");
                setTimeout(function(){
                    hide("wrong");
                },1000);
            }
        }
    }
}
























